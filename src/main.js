// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueLazyload from 'vue-lazyload';
import VuePlayer from 'player';

import App from './App';
import router from './router';
import LazyFov from './components/LazyFov';

Vue.config.productionTip = false;

Vue.use(VuePlayer);
Vue.use(VueLazyload, {
  lazyComponent: true,
});

Vue.component(LazyFov.name, LazyFov);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
});
