import Vue from 'vue';
import Router from 'vue-router';
import Home from '../pages/Home';
// import Big from '../pages/Big';
import Lazy from '../pages/Lazy';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/big',
      name: 'Big',
      component: () => import('../pages/Big'),
    },
    {
      path: '/lazy',
      name: 'Lazy',
      // component: () => import('../pages/Lazy'),
      component: Lazy,
    },
  ],
});
